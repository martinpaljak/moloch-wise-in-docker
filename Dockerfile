ARG WORKERS=1
ARG CONFIGFILE=wiseService.ini

FROM node:10-alpine AS base

FROM base AS builder
WORKDIR /builder
RUN apk add --no-cache su-exec git python make g++
RUN npm install degit
RUN npx degit aol/moloch moloch
# TODO ask Andy to add those to package.json or just read it out from moloch package file
# https://github.com/mmta/docker-wise/blob/master/Dockerfile#L12
# RUN sh -c 'cd moloch/wiseService; \
#      for p in iniparser express async csv request lru-cache bson ioredis \
#      console-stamp morgan connect-timeout elasticsearch splunk-sdk unzip; \
#      do pkg=$(grep $p ../package.json | cut -d\" -f4 | \
#      xargs -I {} npm install --save $p@{}); done'\
#      && npm install --production
RUN cd moloch/wiseService && npm install iniparser ioredis express async csv request lru-cache@4.1.5 console-stamp morgan connect-timeout elasticsearch splunk-sdk unzip bson@1.0.9 && npm install --production

FROM base
USER root
RUN apk add --no-cache tini
RUN mkdir -p /data/moloch/etc && chown -R node:node /data/moloch
USER node
COPY --from=builder --chown=node:node /builder/moloch/wiseService /data/moloch/wiseService
#RUN cp /data/moloch/wiseService/wiseService.ini.sample /data/moloch/etc/wiseService.ini
USER root
COPY ./entrypoint.sh /data/moloch/wiseService/docker-entrypoint.sh
RUN chmod +x /data/moloch/wiseService/docker-entrypoint.sh
USER node

ARG WORKERS
ARG CONFIGFILE

ENV WORKERS ${WORKERS}
ENV CONFIGFILE ${CONFIGFILE}
ENV NODE_ENV production

EXPOSE 8081
WORKDIR /data/moloch/wiseService
ENTRYPOINT ["/sbin/tini", "--", "./docker-entrypoint.sh"]
